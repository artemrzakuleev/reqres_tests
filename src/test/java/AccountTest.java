import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class AccountTest {
    private final String baseURI="https://reqres.in";
    private final String registerPath="/api/register";
    private final String loginPath="/api/login";

    @Test
    public void successfulRegistrationTest()  {
        String email="eve.holt@reqres.in";
        String password="pistol";
        int expectedId=4;
        String expectedToken="QpwL5tke4Pnpja7X4";
        Map<String ,String> requestBody=new HashMap<>();
        requestBody.put("email",email);
        requestBody.put("password",password);
        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post(baseURI+registerPath)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("id",equalTo(expectedId))
                .body("token",equalTo(expectedToken));
    }
    @Test
    public void registerTestWithErrorTest()  {
        String email="michael@abc.ru";
        String password="pass123";
        String expectedError="Note: Only defined users succeed registration";
        Map<String ,String> requestBody=new HashMap<>();
        requestBody.put("email",email);
        requestBody.put("password",password);
        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post(baseURI+registerPath)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("error",equalTo(expectedError));
    }
    @Test
    public void registerWithoutPasswordTest()   {
        String email="michael@abc.ru";
        String expectedError="Missing password";
        Map<String ,String> requestBody=new HashMap<>();
        requestBody.put("email",email);
        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post(baseURI+registerPath)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("error",equalTo(expectedError));
    }
    @Test
    public void successfulLoginTest() {
        String email = "eve.holt@reqres.in";
        String password = "cityslicka";
        String expectedToken = "QpwL5tke4Pnpja7X4";
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("email", email);
        requestBody.put("password", password);
        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post(baseURI + loginPath)
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("token", equalTo(expectedToken));
    }
    @Test
    public void loginWithoutPasswordTest()  {
        String email = "eve.holt@reqres.in";
        String expectedError = "Missing password";
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("email", email);
        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post(baseURI + loginPath)
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("error", equalTo(expectedError));
    }
    @Test
    public void loginWithoutEmailTest() {
        String email = "";
        String password = "123";
        String expectedError = "Missing email or username";
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("email", email);
        requestBody.put("password", password);
        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post(baseURI + loginPath)
                .then()
                .log()
                .body()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .body("error", equalTo(expectedError));
    }
}
