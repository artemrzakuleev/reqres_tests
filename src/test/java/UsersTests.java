import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import io.restassured.http.ContentType;
import model.User;
import model.UserDeserializer;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import java.util.*;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
public class UsersTests {
    private final String baseURI="https://reqres.in";
    private final String basePath="/api/users";

    @Test
    public void getSingleUserTest() {
        User expectedUser=new User(1,"george.bluth@reqres.in","George",
                "Bluth","https://reqres.in/img/faces/1-image.jpg");
        given()
                .pathParams("id",1)
                .get(baseURI+basePath+"/{id}")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .extract()
                .path("data")
                .equals(expectedUser);
    }
    @Test
    public void createUserTest() {
        String name="George";
        String job="Director";
        Map<String,String> requestBody=new HashMap<>();
        requestBody.put("name",name);
        requestBody.put("job",job);
        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .post(baseURI+basePath)
                .then()
                .body("name",equalTo(name))
                .body("job",equalTo(job))
                .statusCode(HttpStatus.SC_CREATED);
    }
    @Test
    public void getMissingUserTest() {
        int missingUserId=23;
        given()
                .pathParams("id",missingUserId)
                .get(baseURI+basePath+"/{id}")
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }
    @Test
    public void deleteUserTest() {
        int id=1;
        given()
                .pathParams("id",id)
                .delete(baseURI+basePath+"/{id}")
                .then()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }
    @Test
    public void putUserTest() {
        String name="George";
        String job="Director";
        Map<String,String> requestBody=new HashMap<>();
        requestBody.put("name",name);
        requestBody.put("job",job);
        int id=1;
        given()
                .pathParams("id",id)
                .contentType(ContentType.JSON)
                .body(requestBody)
                .put(baseURI+basePath+"/{id}")
                .then()
                .body("name",equalTo(name))
                .body("job",equalTo(job))
                .statusCode(HttpStatus.SC_OK);
    }
    @Test
    public void getListUsersTest()  {
        List<User> expectedUsers=new ArrayList<>();
        expectedUsers.add(new User(1,"george.bluth@reqres.in",
                "George","Bluth","https://reqres.in/img/faces/1-image.jpg"));
        expectedUsers.add(new User(2,"janet.weaver@reqres.in", "Janet", "Weaver",
                "https://reqres.in/img/faces/2-image.jpg"));
        expectedUsers.add(new User(3, "emma.wong@reqres.in", "Emma", "Wong",
                "https://reqres.in/img/faces/3-image.jpg"));
        expectedUsers.add(new User(4, "eve.holt@reqres.in", "Eve", "Holt",
                "https://reqres.in/img/faces/4-image.jpg"));
        expectedUsers.add(new User(5, "charles.morris@reqres.in", "Charles", "Morris",
                "https://reqres.in/img/faces/5-image.jpg"));
        expectedUsers.add(new User(6, "tracey.ramos@reqres.in", "Tracey", "Ramos",
                "https://reqres.in/img/faces/6-image.jpg"));
        JsonObject json= get(baseURI+basePath)
                .then()
                .extract()
                .body()
                .as(JsonObject.class);
        JsonArray jsonArray=json.get("data").getAsJsonArray();
        GsonBuilder gsonBuilder=new GsonBuilder();
        gsonBuilder.registerTypeAdapter(User.class, new UserDeserializer());
        Gson gson=gsonBuilder.create();
        List<User> users=gson.fromJson(jsonArray,new TypeToken<List<User>>(){}.getType());
        assertThat(users,equalTo(expectedUsers));
    }
    @Test
    public void getListUsersAndPageTest()  {
        int page=2;
        List<User> expectedUsers=new ArrayList<>();
        expectedUsers.add(new User(7, "michael.lawson@reqres.in", "Michael","Lawson",
                "https://reqres.in/img/faces/7-image.jpg"));
        expectedUsers.add(new User(8, "lindsay.ferguson@reqres.in", "Lindsay", "Ferguson",
                "https://reqres.in/img/faces/8-image.jpg"));
        expectedUsers.add(new User(9, "tobias.funke@reqres.in", "Tobias", "Funke",
                "https://reqres.in/img/faces/9-image.jpg"));
        expectedUsers.add(new User(10, "byron.fields@reqres.in", "Byron", "Fields",
                "https://reqres.in/img/faces/10-image.jpg"));
        expectedUsers.add(new User(11, "george.edwards@reqres.in", "George", "Edwards",
                "https://reqres.in/img/faces/11-image.jpg"));
        expectedUsers.add(new User(12, "rachel.howell@reqres.in", "Rachel", "Howell",
                "https://reqres.in/img/faces/12-image.jpg"));
        JsonObject json=given()
                .queryParam("page",page)
                .get(baseURI+basePath)
                .then()
                .extract()
                .body()
                .as(JsonObject.class);
        JsonArray jsonArray=json.get("data").getAsJsonArray();
        GsonBuilder gsonBuilder=new GsonBuilder();
        gsonBuilder.registerTypeAdapter(User.class, new UserDeserializer());
        Gson gson=gsonBuilder.create();
        List<User> users=gson.fromJson(jsonArray,new TypeToken<List<User>>(){}.getType());
        assertThat(users,equalTo(expectedUsers));
    }

}
