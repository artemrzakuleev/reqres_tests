package model;

import java.util.Objects;

public class User {
    private int id;
    private String email;
    private String first_name;
    private String last_name;
    private String avatarLink;

    public User(int id, String email, String first_name, String last_name, String avatarLink) {
        this.id = id;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.avatarLink = avatarLink;
    }
    public User()   {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public void setAvatarLink(String avatarLink) {
        this.avatarLink = avatarLink;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", avatarLink='" + avatarLink + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && Objects.equals(email, user.email) && Objects.equals(first_name, user.first_name) &&
                Objects.equals(last_name, user.last_name) && Objects.equals(avatarLink, user.avatarLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, first_name, last_name, avatarLink);
    }
}
